
def is_in(letter, iterable):
    for element in iterable:
        if letter == element:
            return True
    return False

vowels = ["a","e","i","o","u"]

def count_vowels(word):
    number_of_vowels = 0
    for letter in word:
        if is_in(letter, vowels):
            number_of_vowels = number_of_vowels + 1
    return number_of_vowels

print(count_vowels("potato"))
