def sum_of_first_n_even_numbers(n):
    if n < 0:
        return None
    sum = 0
    for number in range(n + 1):
        sum = sum + number * 2
    return sum

print(sum_of_first_n_even_numbers((0+1+2+3+4)))