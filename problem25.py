def reverse_dictionary(dictionary):
        different_dict = {}
        for key, value in dictionary.items():
                different_dict[value] = key
        return different_dict

print(reverse_dictionary({"key": "value"}))
