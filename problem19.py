def sum_of_squares(values):
    result = []
    if values == []:
        return None
    for item in values:
        result.append(item**2)
    return sum(result)

print(sum_of_squares([1,2,3,4,5]))
