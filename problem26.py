def add_csv_lines(csv_lines):
    updated_list = []

    for item in csv_lines:
        split_string = item.split(",")
        values = 0
        for item in split_string:
            numbers = int(item)
            values += numbers
        updated_list.append(values)
    return updated_list


print(add_csv_lines(["3","1,9"]))
