necessary_stuff = []
def gear_for_day(is_monday, is_sunny):
    if is_sunny == True and is_monday == True:
        return necessary_stuff + ["laptop"]
    elif is_sunny == True and is_monday != True:
        return necessary_stuff + ["surfboard"]
    elif is_sunny != True and is_monday == True:
        return necessary_stuff + ["umbrella", "laptop"]
    elif is_sunny != True and is_monday != True:
        return necessary_stuff + ["surfboard"]

print(gear_for_day(True, False))
