def count_letters_and_digits(s):
    letter_count = 0
    digit_count = 0

    for item in s:
        if item.isalpha() == True:
            letter_count += 1
        if item.isdigit() == True:
            digit_count += 1
    return letter_count, digit_count

print(count_letters_and_digits("1a"))
