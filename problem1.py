def minimum_value(value1: int, value2: int) -> int:
    # Inputs are two numbers
    # Check which one is the smallest?
    if value1 < value2:
        return value1 # Early Return
        print("After return") # Unreachable code
    #else: # value1 >= value2
    return value2

    # Return the smallest
    # If that are the same return either

def is_num1_larger_than_num2(num1: int, num2: int) -> bool:
    return num1 > num2 # Easier way, comparisons eliminate the need
    if num1 > num2:      # for if/else statements
        return True
    else:
        return False


print(minimum_value(1,3))
print(minimum_value(3,2))
print(minimum_value(3,3))
