def count_letters_and_digits(s):
    # parameter s contains a string that returns two values
    # the number of letters and the number of digits in the string
    # values = ()
    letter_count = 0
    digit_count = 0

    # loop through given string (unpacking)
    #check if the character is a number
        #add one to the counter (+= 1)
    # new if statement: check if it is a letter
        #add one to the counter (+=1)
    for item in s:
        if item.isdigit():
                digit_count += 1
        if item.isalpha():
                letter_count += 1
    return letter_count, digit_count

print(count_letters_and_digits("1"))
