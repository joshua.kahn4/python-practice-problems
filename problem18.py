def find_second_largest(values):
    if values == []:
        return None
    if len(values) == 1:
        return None
    second_highest = set(values)
    second_highest.remove(max(second_highest))
    return max(second_highest)

print(find_second_largest([10,20,30,40,50]))
print(find_second_largest([]))
print(find_second_largest([10]))
