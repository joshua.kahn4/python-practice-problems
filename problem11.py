def has_quorum(attendees_list, members_list):
     half_members = (len(members_list)/2)
     if len(attendees_list) >= half_members:
          return True
     return False
