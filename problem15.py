def calculate_grade(values):
    if values == []:
        return None
    average = sum(values) / len(values)
    if average >= 90:
        return "A"
    if 90 > average >= 80:
        return "B"
    if 80 > average >= 70:
        return "C"
    if 70 > average >= 60:
        return "D"
    return "F"
