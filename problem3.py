Approve1 = True
Approve2 = False
DAge1 = 19
DAge2 = 13
def can_skydive(age, has_consent_form):
    # person has to be greater than or equal to 18
    # or
    # person must have a consent form

   if age >= 18 or has_consent_form == True:
       return True
   else:
        return False

print(can_skydive(DAge1, Approve1))
print(can_skydive(DAge2,Approve2))
print(can_skydive(DAge2,Approve1))
print(can_skydive(DAge1,Approve2))
