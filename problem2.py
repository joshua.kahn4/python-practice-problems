def max_of_three(value1: int, value2: int, value3: int) ->int:
    if value1 >= value2 or value1 >= value3:
        return(value1)
    if value2 >= value3:
        return(value2)
    else:
        return(value3)
