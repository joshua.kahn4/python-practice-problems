def sum_of_first_n_numbers(limit):
    if limit < 0:
        return None
    return sum(range(limit+1))
